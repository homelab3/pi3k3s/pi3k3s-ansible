# pi3k3s-ansible

Ansible project to configure a cluster of 5 Raspberry Pis 3 to run https://github.com/k3s-io/k3s-ansible.

## Run

```bash
ansible-playbook playbooks/pi3k3s.yml -u pi
```

## k3s

Follow https://github.com/k3s-io/k3s-ansible

## kubectl

```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" 
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl 
mkdir ~/.kube
touch ~/.kube/config
scp pi@MASTER_NODE:~/.kube/config ~/.kube/config
```

Test that everything is working
```bash
kubectl get node
```
